package pages;

import builders.ComputerBuilder;
import entities.Computer;
import entities.ComputerDetails;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.RLogger;

import java.util.List;
import java.util.Objects;

import static utils.Properties.computerNameToDelete;

public class HomePage extends YantranetPage {

    @FindBy(id = "add")
    private WebElement addBtn;

    @FindBy(css = ".alert-message.warning")
    private WebElement createdMsg;

    @FindBy(css = ".alert-message.warning")
    private WebElement updatedMsg;

    @FindBy(id = "searchbox")
    private WebElement filterByName;

    @FindBy(id = "searchsubmit")
    private WebElement filterBtn;

    @FindBy(css = ".computers > tbody > tr")
    private List<WebElement> computers;

    @FindBy(css = ".next a")
    private WebElement nextBtn;

    @FindBy(css = ".prev a")
    private WebElement prevBtn;

    @FindBy(css = "#main h1")
    private WebElement numberOfComputer;

    @FindBy(css = ".well em")
    private WebElement noComputersFound;

    @FindBy(partialLinkText = "Cancel")
    private WebElement cancelBtn;


    private RLogger rLogger;
    private Computer computer;
    private ComputerDetails computerDetails;

    private int position = 0;

    public HomePage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
        rLogger =  new RLogger(this);
    }

    public void chooseToCreate(){
        try {
            waitForElementToBeClickable(addBtn, this);
            jsClick(addBtn, driver);
        }catch (TimeoutException e){
            refreshPage(this);
            waitForElementToBeClickable(addBtn, this);
            jsClick(addBtn, driver);
        }
    }

    public void chooseToSearch(){
        search.forEach(searchToRead->searchByNameInDatabase(searchToRead.getComputerName()));

    }

    public void chooseToUpdate(){
        waitForElementsToBeVisible(computers, this);
    }

    public void chooseToDelete(){
        waitForElementsToBeVisible(computers, this);
    }

    public void chooseAComputerToDelete(){
        selectAComputerFromDatabase();
    }

    public void chooseAComputerToUpdate(){
        selectAComputerFromDatabase();
    }

    private void selectAComputerFromDatabase(){
        waitForElementsToBeVisible(computers, this);
        rLogger.info("Identifying a computer...");
        if (computers.size()  < 2) {
            position = 0;
        }
        computer = findAComputer(position);
        rLogger.info("Found a computer to edit -- " + computer.getName());
        try {
            findElement(getComputerElement(position), Yns.computerEditLink).click();
        }catch (Exception e){
            waitForElementToBeClickable(Yns.companyName);
            findElement(getComputerElement(position), Yns.computerEditLink).click();
        }
    }

    public void chooseAComputerFromNextPage(){
        waitForElementsToBeVisible(computers,this);
        waitForElementToBeClickable(nextBtn, this);
        jsClick(nextBtn, driver);
        selectAComputerFromDatabase();
    }

    public void searchAComputerToUpdate(){
        searchByNameInDatabase(update.getComputerName());
        searchMatchDataInDatabaseToUpdate();
    }

    public void searchAComputerToDelete(){
        searchByNameInDatabase(computerNameToDelete);
        if (getTotalNumberOfComputer()==0){
            getErrorMsg();
        }else {
            for (int numberOfMatches = 0; numberOfMatches < getTotalNumberOfComputer(); numberOfMatches++){
                computer = findAComputer(numberOfMatches);
                rLogger.info("capture details");
                String computerName = computer.getName();


                if (computerName.equals(computerNameToDelete)){
                    try {
                        findElement(getComputerElement(numberOfMatches), Yns.computerEditLink).click();
                    }catch (Exception e){
                        waitForElementToBeClickable(Yns.companyName);
                        findElement(getComputerElement(numberOfMatches), Yns.computerEditLink).click();
                    }
                    if (computerName.equals(computerNameToDelete)){
                        break;
                    }
                }
            }
        }
    }

    public Computer getComputer(){
        return computer;
    }

    public void searchMatchDataInDatabase(){
        search.forEach(searchToOperation-> {
        for (int numberOfMatches = 0; numberOfMatches < getTotalNumberOfComputer(); numberOfMatches++){
            computer = findAComputer(numberOfMatches);
            String computerName = computer.getName();
            String searchName = searchToOperation.getComputerName();
            String computerIntroducedDate = computer.getIntroducedDate();
            String searchIntroducedDate = searchToOperation.getIntroducedDate();
            String computerDiscontinuedDate = computer.getDiscontinuedDate();
            String searchDiscontinuedDate = searchToOperation.getDiscontinuedDate();
            String computerCompanyName = computer.getCompany();
            String searchCompanyName = searchToOperation.getCompanyName();

            if (computerName.equals(searchName)
                    && computerIntroducedDate.equals(searchIntroducedDate)
                    && computerDiscontinuedDate.equals(searchDiscontinuedDate)
                    && computerCompanyName.equals(searchCompanyName)){

                try {
                    findElement(getComputerElement(numberOfMatches), Yns.computerEditLink).click();
                }catch (Exception e){
                    waitForElementToBeClickable(Yns.companyName);
                    findElement(getComputerElement(numberOfMatches), Yns.computerEditLink).click();
                }
                if (computerName.equals(searchName)){
                    clickOnCancel();
                    break;
                }
            }
        }});
    }

    private void clickOnCancel(){
        waitForElementToBeClickable(cancelBtn, this);
        jsClick(cancelBtn, driver);
    }

    public void captureDataBeforeOperation(){
        rLogger.info("Capturing data before operations");
        computerDetails = new ComputerDetails();
        computerDetails.setNumberOfComputer(getTotalNumberOfComputer());
    }

    public ComputerDetails getComputerDetailsBeforeOperation(){
        return computerDetails;
    }

    public int getTotalNumberOfComputer(){
        waitForElementToBeVisible(numberOfComputer, this);
        String textFormatOnNumberOfComputer = numberOfComputer.getText().trim().replaceAll("computers found","").replaceAll(" ","");
        if (textFormatOnNumberOfComputer.equals("Onecomputerfound")){
            textFormatOnNumberOfComputer = "1";
        }else if (textFormatOnNumberOfComputer.equals("No"))
            {
            textFormatOnNumberOfComputer = "0";
        }

        int numberOfComputer = Integer.parseInt(textFormatOnNumberOfComputer);
        return numberOfComputer;
    }

    public String getSuccessMsgOnCreated(){
        waitForElementToBeVisible(createdMsg, this);
        String textMsgOnCreated = createdMsg.getText().trim();
        return textMsgOnCreated;
    }

    public String getSuccessMsgOnUpdated(){
        waitForElementToBeVisible(updatedMsg, this);
        String textMsgOnUpdated = updatedMsg.getText().trim();
        return textMsgOnUpdated;
    }


    public void searchByNameInDatabase(String computerName){
        waitForElementToBeClickable(filterByName, this);
        if (!Objects.equals(this.filterByName.getText(), computerName)){
            waitForElementToBeClickable(this.filterByName);
            this.filterByName.clear();
        }
        waitForElementToBeClickable(filterByName, this);
        filterByName.sendKeys(computerName);
        clickOnFilter();
//        selectCreatedComputerName();
    }

    private void clickOnFilter(){
        waitForElementToBeClickable(filterBtn, this);
        jsClick(filterBtn, driver);
    }

    public String getErrorMsg(){
        waitForElementToBeVisible(noComputersFound, this);
        String nothingToDisplay = noComputersFound.getText().trim();
        return nothingToDisplay;
    }

    public Computer getAddedComputer(){
        waitForElementsToBeVisible(computers, this);
        WebElement cartComputerEle = computers.stream().findFirst().get();
        return new AddedComputer(cartComputerEle).getComputer();
    }

    public void searchMatchDataInDatabaseToUpdate(){
        for (int numberOfMatches = 0; numberOfMatches < getTotalNumberOfComputer(); numberOfMatches++){
            computer = findAComputer(numberOfMatches);
            rLogger.info("capture details");
            String computerName = computer.getName();
            String searchName = update.getComputerName();
            String computerIntroducedDate = computer.getIntroducedDate();
            String searchIntroducedDate = update.getIntroducedDate();
            String computerDiscontinuedDate = computer.getDiscontinuedDate();
            String searchDiscontinuedDate = update.getDiscontinuedDate();
            String computerCompanyName = computer.getCompany();
            String searchCompanyName = update.getCompanyName();

            if (computerName.equals(searchName)
                    && computerIntroducedDate.equals(searchIntroducedDate)
                    && computerDiscontinuedDate.equals(searchDiscontinuedDate)
                    && computerCompanyName.equals(searchCompanyName)){

                try {
                    findElement(getComputerElement(numberOfMatches), Yns.computerEditLink).click();
                }catch (Exception e){
                    waitForElementToBeClickable(Yns.companyName);
                    findElement(getComputerElement(numberOfMatches), Yns.computerEditLink).click();
                }
                if (computerName.equals(searchName)){
                    break;
                }
            }else {
                rLogger.info("match not found");
            }
        }
    }


    private class AddedComputer {

        private WebElement computerElement;
        private Computer computer;
        private final By computerName = By.cssSelector(".computers > tbody > tr td:nth-child(1)");
        private final By introducedDate = By.cssSelector(".computers > tbody > tr td:nth-child(2)");
        private final By discontinuedDate = By.cssSelector(".computers > tbody > tr td:nth-child(3)");
        private final By companyName = By.cssSelector(".computers > tbody > tr td:nth-child(4)");
        private final By computerEditLink = By.cssSelector(".computers > tbody > tr td:nth-child(1) a");

        AddedComputer(WebElement computerElement){
            this.computerElement = computerElement;
            setupComputer();
        }

        private void setupComputer(){
            computer = new ComputerBuilder()
                    .withName(getElementText(computerElement, computerName))
                    .withIntroducedDate(getElementText(computerElement, introducedDate))
                    .withDiscontinuedDate(getElementText(computerElement, discontinuedDate))
                    .withCompany(getElementText(computerElement, companyName))
                    .withEditLink(getElementText(computerElement, computerEditLink))
                    .build();
        }

        private Computer getComputer(){
            return computer;
        }
    }
}
