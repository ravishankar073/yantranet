package pages;


import entities.Computer;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utils.RLogger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class AddAComputerPage extends YantranetPage{

    @FindBy(id = "name")
    private WebElement nameOfComputer;

    @FindBy(id = "introduced")
    private WebElement introducedDate;

    @FindBy(id = "discontinued")
    private WebElement discontinuedDate;

    @FindBy(id = "company")
    private WebElement chooseACompany;

    @FindBy(css = ".help-inline")
    private WebElement error;

    @FindBy(css = "input[value='Create this computer']")
    private WebElement createBtn;

    @FindBy(partialLinkText = "Cancel")
    private WebElement cancelBtn;

    @FindBy(className = "help-inline")
    private WebElement customErrorTab;


    private WebDriver driver;
    private Computer addComputer;
    private RLogger rLogger;
    private String computerName;
    private String expected;
    private String actual;

    public AddAComputerPage(WebDriver driver){
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
        rLogger = new RLogger(this);
    }

    public void verifyAddAComputerWithoutEnteringData(){
        waitForElementToBeVisible(nameOfComputer, this);
        if (this.nameOfComputer.getAttribute("value").isEmpty()){
            Assert.assertEquals("Required field", error.getText());
            rLogger.info("Error message appeared..");
        }
    }

    public void addComputer(){
        addComputer = getComputerDetails();
        enterComputerDetails(addComputer);
        clickOnCreate();
    }

    public void addAComputerName(String computerName){
        enterName(computerName);
    }

    public void cancelAComputer(){
        addComputer = getComputerDetails();
        enterComputerDetails(addComputer);
        clickOnCancel();
    }


    private void clickOnCreate(){
        waitForElementToBeClickable(createBtn, this);
        jsClick(createBtn, driver);
    }

    private void clickOnCancel(){
        waitForElementToBeClickable(cancelBtn, this);
        jsClick(cancelBtn, driver);
    }

    public Computer getAddComputerDetails(){
        return addComputer;
    }

    private void enterComputerDetails(Computer computer){
        String getName = computer.getName();
        String getIntroducedDate = computer.getIntroducedDate();
        String getDiscontinuedDate = computer.getDiscontinuedDate();
        enterName(getName);
        enterIntroducedDate(getIntroducedDate);
        enterDiscontinuedDate(getDiscontinuedDate);
        computer.getCompany();
    }

    public void enterName(String name){
        waitForElementToBeVisible(nameOfComputer, this);
        rLogger.info("Entering computer name--"+name);
        this.nameOfComputer.sendKeys(name);
    }

    public void verifyName(){
        waitForElementToBeVisible(nameOfComputer, this);
        if (!StringUtils.isNumeric(nameOfComputer.getAttribute("value"))){
            Assert.fail("Name should not contain number");
        }
    }

    public void enterIntroducedDate(String introduceDate){
        waitForElementToBeVisible(introducedDate, this);
        rLogger.info("Entering introduced date--"+introduceDate);
//        jsSetValOnElement(introducedDate,driver,introduceDate);
        this.introducedDate.sendKeys(introduceDate);
    }

    public void verifyIntroducedDate() throws ParseException {
        waitForElementToBeVisible(introducedDate, this);
        Date targetDateForCampaign =new SimpleDateFormat("YYYY-MM-DD").parse((this.introducedDate.getAttribute("value")));
        if (targetDateForCampaign.before(new Date())){
            Assert.fail("Enter a valid date");
        }
    }

    public void enterDiscontinuedDate(String discontinueDate){
        waitForElementToBeVisible(discontinuedDate, this);
        rLogger.info("Entering introduced date--"+discontinueDate);
//        jsSetValOnElement(discontinuedDate,driver,discontinueDate);
        this.discontinuedDate.sendKeys(discontinueDate);
    }

    public void verifyDiscontinuedDate() throws ParseException {
        waitForElementToBeVisible(discontinuedDate, this);
        Date targetDateForCampaign =new SimpleDateFormat("YYYY-MM-DD").parse((this.discontinuedDate.getAttribute("value")));
        if (targetDateForCampaign.before(new Date())){
            Assert.fail("Enter a valid date");
        }
    }

    protected String selectByCompany(){
        String selectedOption;
        try {
            waitForElementToBeVisible(chooseACompany, this);
            selectOption(chooseACompany, randomSort());
            selectedOption = getSelectedValue(chooseACompany);
        }catch (Exception e){
            refreshPage(this);
            waitForElementToBeVisible(chooseACompany, this);
            selectOption(chooseACompany, randomSort());
            selectedOption = getSelectedValue(chooseACompany);
        }
        rLogger.info("Choosing random company name--" + selectedOption);
        return selectedOption;
    }

    protected int randomSort() {
        Random rand = new Random();
        int min = 0;
        int max = 43;
        return rand.nextInt((max - min) + 1) + min;
    }

    public void assertionForEmptyFields() {
        expected = "Required";
        actual = customErrorTab.getText();
        org.junit.Assert.assertEquals(expected, actual);
        rLogger.info("Message verified");
    }

}
