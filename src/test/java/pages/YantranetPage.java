package pages;


import builders.ComputerBuilder;
import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;
import entities.Computer;
import entities.operationsdata.OperationsData;
import entities.operationsdata.SearchDetails;
import entities.operationsdata.UpdateDetails;
import org.apache.commons.lang.time.DateUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.PropertiesHelper;
import utils.RandomDataGenerator;

import java.util.Date;
import java.util.List;

import static utils.JsonMapper.getDetails;
import static utils.Properties.timeout;

public class YantranetPage {

    @FindBy(css = ".computers > tbody > tr")
    private List<WebElement> computers;

    private WebDriverWait wait;
    protected WebDriver driver;
    protected List<SearchDetails> search;
    protected UpdateDetails update;


    private PropertiesHelper propertiesHelper = new PropertiesHelper();

    public YantranetPage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, timeout);
        OperationsData data = getDetails();
        this.search = data.getSearch();
        this.update = data.getUpdate();
    }

    public void waitForElementToBeVisible(WebElement element){
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitForElementToBeClickable(WebElement element){
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitForElementToBeVisible(WebElement element, Object obj){
        try {
            wait.until(ExpectedConditions.visibilityOf(element));
        }catch (StaleElementReferenceException e){
            refreshPage(obj);
            wait.until(ExpectedConditions.visibilityOf(element));
        }
    }

    public void waitForElementsToBeVisible(List<WebElement> elements, Object obj) {
        try {
            wait.until(ExpectedConditions.visibilityOfAllElements(elements));
        } catch (StaleElementReferenceException e) {
            refreshPage(obj);
            wait.until(ExpectedConditions.visibilityOfAllElements(elements));
        }
    }

    public void waitForElementToBeClickable(WebElement element, Object obj){
        try {
            wait.until(ExpectedConditions.elementToBeClickable(element));
        }catch (StaleElementReferenceException e){
            refreshPage(obj);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        }
    }

    public void waitForElementToBeVisible(WebElement element, int timeout){
        WebDriverWait webDriverWait = new WebDriverWait(driver, timeout);
        webDriverWait.until(ExpectedConditions.visibilityOf(element));
    }

    protected void waitForElementToBeClickable(By element) {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(element));
    }

    protected void jsClick(WebElement webElement, WebDriver webDriver){
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", webElement);
    }

    protected void jsSetValOnElement(WebElement webElement, WebDriver webDriver, String val){
        ((JavascriptExecutor) webDriver).executeScript("$(arguments[0]).val(arguments[1]);", webElement, val);
    }

    protected String getElementText(WebElement parentElement, By childElement){
        return parentElement.findElement(childElement).getText();
    }

    protected WebElement findElement(WebElement element, By by){
        return element.findElement(by);
    }

    protected WebElement findElement(List<WebElement> elements){
        return elements.stream().findFirst().get();
    }

    protected WebElement findElement(By by){
        return driver.findElement(by);
    }

    protected String getText(WebElement webElement){
        return webElement.getText();
    }

    protected String getSelectedValue(WebElement selectElement){
        Select select = new Select(selectElement);
        return select.getFirstSelectedOption().getText();
    }

    protected void seletOption(WebElement element, String option){
        Select select = new Select(element);
        select.selectByVisibleText(option);
    }

    protected void selectOption(WebElement element, int index){
        Select select = new Select(element);
        select.selectByIndex(index);
    }

    public boolean isElementVisible(WebElement element){
        try {
            waitForElementToBeVisible(element,3);
            return true;
        }catch (Exception e){
            return false;
        }
    }
    protected void refreshPage(Object obj){
        PageFactory.initElements(driver, obj);
    }

    public WebDriver getDriver(){
        return driver;
    }

    Computer getComputerDetails(){
        Computer computer = new Computer();
        RandomDataGenerator rd = new RandomDataGenerator();
        computer.setName(rd.getRandomName());
        Date startDate = rd.getRandomDate(new Date());
        computer.setIntroducedDate(rd.formatDate(startDate));

        Date discontinuedDate = rd.getRandomDate(startDate);
        computer.setDiscontinuedDate(rd.formatDate(discontinuedDate));

        AddAComputerPage add = new AddAComputerPage(driver);
        computer.setCompany(add.selectByCompany());
        return computer;
    }

    Computer getEditComputerDetails(){
        Computer computer = new Computer();
        RandomDataGenerator rd = new RandomDataGenerator();
        computer.setEditName(rd.getRandomName());
        Date startDate = rd.getRandomDate(new Date());
        computer.setEditIntroducedDate(rd.formatDate(startDate));

        Date discontinuedDate = rd.getRandomDate(startDate);
        computer.setEditDiscontinuedDate(rd.formatDate(discontinuedDate));

        AddAComputerPage add = new AddAComputerPage(driver);
        computer.setEditCompany(add.selectByCompany());
        return computer;
    }

    Computer findAComputer(int computerCount){
        WebElement computerEle = getComputerElement(computerCount);
        return buildComputer(computerEle);
    }

    protected WebElement getComputerElement(int computerCount){
        try {
            waitForElementsToBeVisible(computers, this);
            return computers.get(computerCount);
        }catch (StaleElementReferenceException e){
            refreshPage(this);
            waitForElementsToBeVisible(computers, this);
            return computers.stream().findAny().get();
        }
    }

    Computer buildComputer(WebElement computerEle){
        waitForElementsToBeVisible(computers, this);
        return new ComputerBuilder()
                 .withName(getElementText(computerEle, Yns.computerName))
                .withIntroducedDate(getElementText(computerEle, Yns.introducedName))
                .withDiscontinuedDate(getElementText(computerEle, Yns.discontinuedDate))
                .withCompany(getElementText(computerEle, Yns.companyName))
                .build();
    }

    protected interface Yns {
        By computerName = By.cssSelector(".computers > tbody > tr td:nth-child(1)");
        By introducedName = By.cssSelector(".computers > tbody > tr td:nth-child(2)");
        By discontinuedDate = By.cssSelector(".computers > tbody > tr td:nth-child(3)");
        By companyName = By.cssSelector(".computers > tbody > tr td:nth-child(4)");
        By computerEditLink = By.cssSelector(".computers > tbody > tr td:nth-child(1) a");
    }
}
