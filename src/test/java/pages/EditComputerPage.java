package pages;


import entities.Computer;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.RLogger;

import java.util.List;

public class EditComputerPage extends YantranetPage {

    @FindBy(css = ".computers > tbody > tr")
    private List<WebElement> computers;

    @FindBy(id = "name")
    private WebElement editName;

    @FindBy(id = "introduced")
    private WebElement editIntroducedDate;

    @FindBy(id = "discontinued")
    private WebElement editDiscontinuedDate;

    @FindBy(id = "company")
    private WebElement editCompany;

    @FindBy(css = "input[value='Save this computer']")
    private WebElement saveBtn;

    @FindBy(css = ".main h1")
    private WebElement editComputer;

    @FindBy(css = "input[value='Delete this computer']")
    private WebElement deleteBtn;

    private WebDriver driver;
    private RLogger rLogger;
    private Computer editAComputer;

    private int position = 0;

    public EditComputerPage(WebDriver driver){
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
        rLogger = new RLogger(this);
    }



    public void editName(String name){
        waitForElementToBeVisible(editName);
        rLogger.info("Edit computer name--" +name);
        editName.clear();
        this.editName.sendKeys(name);
    }

    public void editIntroducedDate(String introducedDate){
        String date = introducedDate;
        System.out.println(date);
        waitForElementToBeVisible(editIntroducedDate, this);
        rLogger.info("Edit introduced date--" +introducedDate);
        editIntroducedDate.clear();
        this.editIntroducedDate.sendKeys(introducedDate);
    }

    public void editDiscontinuedDate(String discontinuedDate){
        waitForElementToBeVisible(editDiscontinuedDate, this);
        rLogger.info("Edit discontinued date--" +discontinuedDate);
        editDiscontinuedDate.clear();
        this.editDiscontinuedDate.sendKeys(discontinuedDate);
    }

    public void ediCompanyName(String company){
        waitForElementToBeVisible(editCompany, this);
        rLogger.info("Edit company Name--" +company);
        this.editCompany.sendKeys(company);
    }

    public void editComputerDetails(){
//        waitForElementToBeVisible(editComputer, this);
        editAComputer = getEditComputerDetails();
        enterDetailsToUpdate(editAComputer);
        clickOnSave();
    }

    private void enterDetailsToUpdate(Computer computer){
        String computerName = computer.getEditName();
        String introducedDate = computer.getEditIntroducedDate();
        String discontinuedDate = computer.getEditDiscontinuedDate();
        String companyName = computer.getEditCompany();

        editName(computerName);
        editIntroducedDate(introducedDate);
        editDiscontinuedDate(discontinuedDate);
        ediCompanyName(companyName);
    }

    public Computer getEditedComputerDetails(){
        return editAComputer;
    }


    private void clickOnSave(){
        if (isElementVisible(saveBtn)){
            waitForElementToBeClickable(saveBtn, this);
            jsClick(saveBtn, driver);
        }
    }

    public void clickOnDelete(){
       if (isElementVisible(deleteBtn)){
           waitForElementToBeClickable(deleteBtn, this);
           jsClick(deleteBtn, driver);
       }
    }

    public void checkUpdatedComputerDetailsInDatabase(){

    }
}
