package dataprovider;


import org.testng.annotations.DataProvider;

public class NegativeTestCases {

    @DataProvider(name = "noData")
    public static Object[][] noDataToEnter(){
        return new Object[][]{
                {"computerName"}
        };
    }

    @DataProvider(name = "verifyName")
    public static Object[][] verifyName(){
        return new Object[][]{
                //computerName,introducedDate,discontinuedDate
                {""}
        };
    }
}
