package dataprovider;


import org.testng.annotations.DataProvider;

import static utils.OperationType.*;

public class YantranetDataForOperation {

    @DataProvider(name = "addANewComputerToComputerDatabase")
    public static Object[][] addANewComputerToComputerDatabase(){
        return new Object[][]{
                {CREATE},
        };
    }

    @DataProvider(name = "readANewComputerDetailsFromComputerDatabase")
    public static Object[][] readANewComputerFromComputerDatabase(){
        return new Object[][]{
                {READ},
        };
    }

    @DataProvider(name = "updateAComputerDetailsInComputerDatabase")
    public static Object[][] updateANewComputerInComputerDatabase(){
        return new Object[][]{
                {UPDATE}
        };
    }

    @DataProvider(name = "deleteAComputerDetailsFromComputerDatabase")
    public static Object[][] deleteANewComputerFromComputerDatabase(){
        return new Object[][]{
                {DELETE}
        };
    }
}
