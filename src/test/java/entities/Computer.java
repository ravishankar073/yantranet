package entities;


import javax.print.DocFlavor;

public class Computer {

    private String name;
    private String introducedDate;
    private String discontinuedDate;
    private String company;
    private String editComputerLink;

    private String editName;
    private String editIntroducedDate;
    private String editDiscontinuedDate;
    private String editCompany;

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setIntroducedDate(String introducedDate){
        this.introducedDate = introducedDate;
    }

    public String getIntroducedDate(){
        return introducedDate;
    }

    public void setDiscontinuedDate(String discontinuedDate){
        this.discontinuedDate = discontinuedDate;
    }

    public String getDiscontinuedDate(){
        return discontinuedDate;
    }

    public void setCompany(String company){
        this.company = company;
    }

    public String getCompany(){
        return company;
    }

    public void setEditComputerLink(String editComputerLink){
        this.editComputerLink = editComputerLink;
    }
    public String getEditComputerLink(){
        return editComputerLink;
    }

    public void setEditName(String editName){
        this.editName = editName;
    }

    public String getEditName(){
        return editName;
    }

    public void setEditIntroducedDate(String editIntroducedDate){
        this.editIntroducedDate = editIntroducedDate;
    }

    public String getEditIntroducedDate(){
        return editIntroducedDate;
    }

    public void setEditDiscontinuedDate(String editDiscontinuedDate){
        this.editDiscontinuedDate = editDiscontinuedDate;
    }

    public String getEditDiscontinuedDate(){
        return editDiscontinuedDate;
    }

    public void setEditCompany(String editCompany){
        this.editCompany = editCompany;
    }

    public String getEditCompany(){
        return editCompany;
    }

}
