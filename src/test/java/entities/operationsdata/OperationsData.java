package entities.operationsdata;


import java.util.List;

public class OperationsData {

    private List<SearchDetails> search;
    private UpdateDetails update;

    public List<SearchDetails> getSearch(){
        return search;
    }

    public void setSearch(List<SearchDetails> search){
        this.search = search;
    }

    public UpdateDetails getUpdate(){
        return update;
    }

    public void setUpdate(UpdateDetails update){
        this.update = update;
    }
}
