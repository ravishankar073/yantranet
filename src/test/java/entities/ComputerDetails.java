package entities;


public class ComputerDetails {

    private int numberOfComputer;

    public int getNumberOfComputer(){
        return numberOfComputer;
    }

    public void setNumberOfComputer(int numberOfComputer){
        this.numberOfComputer = numberOfComputer;
    }
}
