package tests;


import entities.Computer;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pages.AddAComputerPage;
import pages.EditComputerPage;
import pages.HomePage;
import utils.DriverFactory;
import utils.PageStore;
import utils.RLogger;

import static utils.Properties.url;

public class Yantranet {

    protected WebDriver driver;
    protected RLogger rLogger;
    private DriverFactory driverFactory;
    protected PageStore pageStore;

    public Yantranet(){
        rLogger = new RLogger(this);
    }

    @BeforeMethod(alwaysRun = true)
    public synchronized void setup(){
        rLogger.info("setting up driver");
        driverFactory = new DriverFactory();
        driver = driverFactory.getDriver();
        driver.get(url);
        pageStore = new PageStore(driver);
    }

    @AfterMethod(alwaysRun = true)
    public synchronized void teardown(){
        driver.quit();
    }

    protected void addANewComputer(){
        AddAComputerPage add = pageStore.get(AddAComputerPage.class);
        Computer computer = new Computer();
        rLogger.info("Adding a new computer to database--"+computer.getName());
        add.addComputer();
    }

    protected void fillDetailsAndClickOnCancel(){
        AddAComputerPage add = pageStore.get(AddAComputerPage.class);
        Computer computer = new Computer();
        rLogger.info("Fill a computer details and click on cancel--"+computer.getName());
        add.cancelAComputer();
    }

    protected void editAComputer(){
        EditComputerPage edit = pageStore.get(EditComputerPage.class);
        Computer computer = new Computer();
        rLogger.info("Edit a computer details in database--"+computer.getEditName());
        edit.editComputerDetails();
    }

    protected void deleteAComputer(){
        EditComputerPage edit = pageStore.get(EditComputerPage.class);
        HomePage homePage = pageStore.get(HomePage.class);
        Computer computer = new Computer();
        rLogger.info("delete a computer details from database--"+computer.getName());
        edit.clickOnDelete();
    }

    public WebDriver getDriver(){
        return pageStore.getDriver();
    }
}
