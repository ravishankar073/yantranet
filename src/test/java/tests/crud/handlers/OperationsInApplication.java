package tests.crud.handlers;


import pages.HomePage;
import utils.Assertions;
import utils.OperationType;

import java.text.ParseException;

public class OperationsInApplication  extends Operations{

    protected void createAComputerDetails(OperationType operationType){
        Assertions assertions = pageStore.get(Assertions.class);
        assertions.verifyHomePage();
        chooseOperation(operationType);
        addANewComputer();
        verifyAddedComputer();
    }


    protected void createAComputerDetailsAndVerifyInDatabase(OperationType operationType) throws ParseException {
        Assertions assertions = pageStore.get(Assertions.class);
        assertions.verifyHomePage();
        chooseOperation(operationType);
        addANewComputer();
        verifyCreatedComputerInDatabase();
    }

    protected void createAComputerDetailsAndCheckCountInDatabase(OperationType operationType){
        Assertions assertions = pageStore.get(Assertions.class);
        assertions.verifyHomePage();
        chooseOperation(operationType);
        addANewComputer();
        verifyNumberOfComputerAfterCreation();
    }

    protected void createAComputerDetailsAndClickOnCancel(OperationType operationType){
        Assertions assertions = pageStore.get(Assertions.class);
        assertions.verifyHomePage();
        chooseOperation(operationType);
        fillDetailsAndClickOnCancel();
        verifyComputerDetailsOnCancel();
    }

    protected void readAComputerDetails(OperationType operationType){
        Assertions assertions = pageStore.get(Assertions.class);
        assertions.verifyHomePage();
        chooseOperation(operationType);
        verifySearchDetails();
    }

    protected void readComputerDetailsAfterCreation(OperationType operationType) throws ParseException {
        Assertions assertions = pageStore.get(Assertions.class);
        assertions.verifyHomePage();
        chooseOperation(operationType);
        addANewComputer();
        verifyCreatedComputerInDatabase();
    }

    protected void updateComputerDetails(OperationType operationType){
        HomePage homePage = pageStore.get(HomePage.class);
        Assertions assertions = pageStore.get(Assertions.class);
        assertions.verifyHomePage();
        chooseOperation(operationType);
        homePage.chooseAComputerToUpdate();
        editAComputer();
        verifyUpdatedComputer();
    }

    protected void updateComputerDetailsAndCheckNumber(OperationType operationType){
        HomePage homePage = pageStore.get(HomePage.class);
        Assertions assertions = pageStore.get(Assertions.class);
        assertions.verifyHomePage();
        chooseOperation(operationType);
        homePage.chooseAComputerToUpdate();
        editAComputer();
        verifyNumberOfComputerAfterUpdate();
    }

    protected void updateComputerDetailsAndCheckInDatabase(OperationType operationType) throws ParseException {
        HomePage homePage = pageStore.get(HomePage.class);
        Assertions assertions = pageStore.get(Assertions.class);
        assertions.verifyHomePage();
        chooseOperation(operationType);
        homePage.chooseAComputerToUpdate();
        editAComputer();
        verifyUpdatedComputerDetailsInDatabase();
    }

    protected void chooseAComputerToUpdateFromDifferentPage(OperationType operationType) throws ParseException {
        HomePage homePage = pageStore.get(HomePage.class);
        Assertions assertions = pageStore.get(Assertions.class);
        assertions.verifyHomePage();
        chooseOperation(operationType);
        homePage.chooseAComputerFromNextPage();
        editAComputer();
        verifyUpdatedComputerDetailsInDatabase();
    }

    protected void searchAComputerToUpdateInDatabase(OperationType operationType) throws ParseException {
        HomePage homePage = pageStore.get(HomePage.class);
        Assertions assertions = pageStore.get(Assertions.class);
        assertions.verifyHomePage();
        chooseOperation(operationType);
        homePage.searchAComputerToUpdate();
        editAComputer();
        verifyUpdatedComputerDetailsInDatabase();
    }

    protected void updateComputerDetailsAndVerifyWithExistingDetails(OperationType operationType){
        chooseOperation(operationType);
        editAComputer();
        verifyUpdatedWithExistingComputerDetails();
    }

    protected void updateComputerDetailsAndCheckWithExistingDetailsInDatabase(OperationType operationType){
        chooseOperation(operationType);
        verifyExistingDataInDatabaseAfterUpdate();
    }

    protected void deleteAComputerDetailsFromDatabase(OperationType operationType){
        HomePage homePage = pageStore.get(HomePage.class);
        Assertions assertions = pageStore.get(Assertions.class);
        assertions.verifyHomePage();
        chooseOperation(operationType);
        homePage.chooseAComputerToDelete();
        deleteAComputer();
        verifyComputerDetailsAfterDeletion();
    }

    protected void searchAComputerAndDeleteFromDatabase(OperationType operationType){
        HomePage homePage = pageStore.get(HomePage.class);
        Assertions assertions = pageStore.get(Assertions.class);
        assertions.verifyHomePage();
        chooseOperation(operationType);
        homePage.searchAComputerToDelete();
        deleteAComputer();
        verifyErrorMessageOnDelete();
    }
}
