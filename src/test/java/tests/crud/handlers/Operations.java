package tests.crud.handlers;

import entities.Computer;
import entities.ComputerDetails;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import pages.AddAComputerPage;
import pages.EditComputerPage;
import pages.HomePage;
import tests.Yantranet;
import utils.OperationType;
import utils.RLogger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Operations extends Yantranet{

    protected  AddAComputerPage addAComputerPage;
    protected EditComputerPage editComputerPage;
    protected HomePage homePage;

    @BeforeMethod
    public synchronized void setup(){
        super.setup();
        addAComputerPage = pageStore.get(AddAComputerPage.class);
        editComputerPage = pageStore.get(EditComputerPage.class);
        homePage = pageStore.get(HomePage.class);
    }

    public Operations(){
        rLogger = new RLogger(this.getClass());
    }

    void chooseOperation(OperationType operationType){
        HomePage homePage = pageStore.get(HomePage.class);
        switch (operationType){
            case CREATE:
                rLogger.info("choosing operation to create a computer details");
                homePage.captureDataBeforeOperation();
                homePage.chooseToCreate();
                break;
            case READ:
                rLogger.info("choosing operation to read a computer details");
                homePage.chooseToSearch();
                break;
            case UPDATE:
                rLogger.info("choosing operation to update a computer details");
                homePage.captureDataBeforeOperation();
                homePage.chooseToUpdate();
                break;
            case DELETE:
                rLogger.info("choosing operation to delete a computer details");
                homePage.captureDataBeforeOperation();
                homePage.chooseToDelete();
        }
    }

    void verifyAddedComputer(){
        Computer computer = addAComputerPage.getAddComputerDetails();
        String createdComputerName = computer.getName();
        rLogger.info("Verify success message after adding to database");
        Assert.assertEquals("Done! Computer " + createdComputerName +" has been created", homePage.getSuccessMsgOnCreated());
    }

    void verifyCreatedComputerInDatabase() throws ParseException {
        Computer computer = addAComputerPage.getAddComputerDetails();
        String createdComputerName = computer.getName();
        rLogger.info("Filter by created name");
        homePage.searchByNameInDatabase(createdComputerName);
        Computer addedComputer = homePage.getAddedComputer();

        //To change Date format
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat targetFormat = new SimpleDateFormat("dd MMM yyyy");

        Date actualIntroducedDate = originalFormat.parse(computer.getIntroducedDate());
        String introducedFormattedDate = targetFormat.format(actualIntroducedDate);

        Date actualDiscontinuedDate = originalFormat.parse(computer.getDiscontinuedDate());
        String discontinuedFormattedDate = targetFormat.format(actualDiscontinuedDate);

        rLogger.info("Verify new computer details added to database");
        Assert.assertEquals(computer.getName(), addedComputer.getName());
        Assert.assertEquals(introducedFormattedDate, addedComputer.getIntroducedDate());
        Assert.assertEquals(discontinuedFormattedDate, addedComputer.getDiscontinuedDate());
        Assert.assertEquals(computer.getCompany(), addedComputer.getCompany());
    }

    protected void verifyComputerDetailsOnCancel(){
        Computer computer = addAComputerPage.getAddComputerDetails();
        String createdComputerName = computer.getName();
        rLogger.info("Filter by created name");
        homePage.searchByNameInDatabase(createdComputerName);

        rLogger.info("Verify new computer details added to database");
        Assert.assertEquals("Nothing to display", homePage.getErrorMsg());
    }

    protected void verifyNumberOfComputerAfterCreation(){
        ComputerDetails computerDetails = homePage.getComputerDetailsBeforeOperation();
        rLogger.info("Verify count of number after creation");
        int noOfComputerBeforeOperation = computerDetails.getNumberOfComputer();
        System.out.println(noOfComputerBeforeOperation);
        int noOfComputerInHomepage = homePage.getTotalNumberOfComputer();
        System.out.println(noOfComputerInHomepage);
        Assert.assertEquals(computerDetails.getNumberOfComputer()+1, homePage.getTotalNumberOfComputer());
    }

    protected void verifySearchDetails(){
        rLogger.info("Verified  search result successfully");
        homePage.searchMatchDataInDatabase();
    }




    void verifyUpdatedComputer(){
        Computer computer = editComputerPage.getEditedComputerDetails();
        String updatedComputerName = computer.getEditName();
        rLogger.info("Verify success message after adding to database");
        Assert.assertEquals("Done! Computer " + updatedComputerName +" has been updated", homePage.getSuccessMsgOnUpdated());
    }



    void verifyUpdatedComputerDetailsInDatabase() throws ParseException {
        Computer computer = editComputerPage.getEditedComputerDetails();
        String editedComputerName = computer.getEditName();
        rLogger.info("Filter by edited name");
        homePage.searchByNameInDatabase(editedComputerName);
        Computer computerInList = homePage.getAddedComputer();

        //To change Date format
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat targetFormat = new SimpleDateFormat("dd MMM yyyy");

        Date actualIntroducedDate = originalFormat.parse(computer.getEditIntroducedDate());
        String introducedFormattedDate = targetFormat.format(actualIntroducedDate);

        Date actualDiscontinuedDate = originalFormat.parse(computer.getEditDiscontinuedDate());
        String discontinuedFormattedDate = targetFormat.format(actualDiscontinuedDate);

        rLogger.info("Verify updated computer details in database");
        Assert.assertEquals(computer.getEditName(), computerInList.getName());
        Assert.assertEquals(introducedFormattedDate, computerInList.getIntroducedDate());
        Assert.assertEquals(discontinuedFormattedDate, computerInList.getDiscontinuedDate());
        Assert.assertEquals(computer.getEditCompany(), computerInList.getCompany());
    }

    protected void verifyNumberOfComputerAfterUpdate(){
        ComputerDetails computerDetails = homePage.getComputerDetailsBeforeOperation();
        rLogger.info("Verify count of number after creation");
        Assert.assertEquals(computerDetails.getNumberOfComputer(), homePage.getTotalNumberOfComputer());
    }

    protected void verifyUpdatedComputerDetailsOnCancel(){
        Computer computer = editComputerPage.getEditedComputerDetails();
         String editedComputerName = computer.getEditName();
        rLogger.info("Filter by created name");
        homePage.searchByNameInDatabase(editedComputerName);

        rLogger.info("Verify new computer details added to database");
        Assert.assertEquals("Nothing to display", homePage.getErrorMsg());
    }

    protected void verifyUpdatedWithExistingComputerDetails(){
        Computer computer = homePage.getComputer();
        String chooseComputerToEdit = computer.getName();
        System.out.println(chooseComputerToEdit);

        Computer editComputer = editComputerPage.getEditedComputerDetails();
        String editedComputerName = editComputer.getEditName();
        System.out.println(editedComputerName);

        Assert.assertNotEquals(computer, editComputer);
    }

    protected void verifyExistingDataInDatabaseAfterUpdate(){
        Computer computer = homePage.getComputer();
        EditComputerPage edit = pageStore.get(EditComputerPage.class);
        String chooseComputerToEdit = computer.getName();
        edit.editComputerDetails();
        homePage.searchByNameInDatabase(chooseComputerToEdit);
        Assert.assertEquals("Nothing to display", homePage.getErrorMsg());
    }


    protected void verifyComputerDetailsAfterDeletion(){
        ComputerDetails computerDetails = homePage.getComputerDetailsBeforeOperation();
        rLogger.info("Verify count of number after creation");
        if (computerDetails.getNumberOfComputer() == 0){
            Assert.assertEquals(computerDetails.getNumberOfComputer(), homePage.getTotalNumberOfComputer());
        }else {
            Assert.assertEquals(computerDetails.getNumberOfComputer()-1, homePage.getTotalNumberOfComputer());
        }
    }

    protected void verifyErrorMessageOnDelete(){
        Assert.assertEquals("Nothing to display", homePage.getErrorMsg());
    }


}
