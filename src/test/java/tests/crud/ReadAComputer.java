package tests.crud;


import dataprovider.YantranetDataForOperation;
import org.testng.annotations.Test;
import tests.crud.handlers.OperationsInApplication;
import utils.OperationType;

import java.text.ParseException;

public class ReadAComputer extends OperationsInApplication {

    @Test(dataProvider = "readANewComputerDetailsFromComputerDatabase", dataProviderClass = YantranetDataForOperation.class, groups = {"regression"})
    public void readAComputer(OperationType operationType){
        readAComputerDetails(operationType);
    }

    @Test(dataProvider = "addANewComputerToComputerDatabase", dataProviderClass = YantranetDataForOperation.class, groups = {"regression"})
    public void readAComputerDetailsAfterCreation(OperationType operationType) throws ParseException {
        readComputerDetailsAfterCreation(operationType);
    }
}
