package tests.crud;

import dataprovider.YantranetDataForOperation;
import org.testng.annotations.Test;
import tests.crud.handlers.OperationsInApplication;
import utils.OperationType;


public class DeleteAComputer extends OperationsInApplication {

    @Test(dataProvider = "deleteAComputerDetailsFromComputerDatabase", dataProviderClass = YantranetDataForOperation.class)
    public void deleteAComputerDetailsFromComputerDatabase(OperationType operationType){
        deleteAComputerDetailsFromDatabase(operationType);
    }

    @Test(dataProvider = "deleteAComputerDetailsFromComputerDatabase", dataProviderClass = YantranetDataForOperation.class)
    public void searchAComputerDetailsAndDeleteFromDatabase(OperationType operationType){
        searchAComputerAndDeleteFromDatabase(operationType);
    }
}
