package tests.crud;


import dataprovider.YantranetDataForOperation;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import tests.crud.handlers.OperationsInApplication;
import utils.OperationType;

import java.text.ParseException;

public class UpdateAComputer extends OperationsInApplication {

    @Test(dataProvider = "updateAComputerDetailsInComputerDatabase", dataProviderClass = YantranetDataForOperation.class)
    public void editAComputerDetailsInDatabase(OperationType operationType){
        updateComputerDetails(operationType);
    }

    @Test(dataProvider = "updateAComputerDetailsInComputerDatabase", dataProviderClass = YantranetDataForOperation.class)
    public void editAComputerDetailsAndVerifyInDatabase(OperationType operationType) throws ParseException {
        updateComputerDetailsAndCheckInDatabase(operationType);
    }

    @Test(dataProvider = "updateAComputerDetailsInComputerDatabase", dataProviderClass = YantranetDataForOperation.class)
    public void editAComputerDetailsAndVerifyNumberOfComputerInDatabase(OperationType operationType) throws ParseException {
        updateComputerDetailsAndCheckNumber(operationType);
    }

    @Test(dataProvider = "updateAComputerDetailsInComputerDatabase", dataProviderClass = YantranetDataForOperation.class)
    public void editAComputerDetailsFromNextPageAndVerifyInComputerInDatabase(OperationType operationType) throws ParseException {
        chooseAComputerToUpdateFromDifferentPage(operationType);
    }

    //You can run this test by providing data in operations.json
     @Test(dataProvider = "updateAComputerDetailsInComputerDatabase", dataProviderClass = YantranetDataForOperation.class)
    public void searchAComputerToEditInDatabase(OperationType operationType) throws ParseException {
        searchAComputerToUpdateInDatabase(operationType);
    }
}
