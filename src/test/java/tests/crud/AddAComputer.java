package tests.crud;


import dataprovider.YantranetDataForOperation;
import org.testng.annotations.Test;
import tests.crud.handlers.OperationsInApplication;
import utils.OperationType;

import java.text.ParseException;

public class  AddAComputer extends OperationsInApplication{

    @Test(dataProvider = "addANewComputerToComputerDatabase", dataProviderClass = YantranetDataForOperation.class)
    public void addANewComputerToDatabase(OperationType operationType){
        createAComputerDetails(operationType);
    }

    @Test(dataProvider = "addANewComputerToComputerDatabase", dataProviderClass = YantranetDataForOperation.class, groups = {"regression"})
    public void addANewComputerAndVerifyInDatabase(OperationType operationType) throws ParseException {
        createAComputerDetailsAndVerifyInDatabase(operationType);
    }

    @Test(dataProvider = "addANewComputerToComputerDatabase", dataProviderClass = YantranetDataForOperation.class, groups = {"regression"})
    public void addANewComputerAndCheckCountInDatabase(OperationType operationType) {
        createAComputerDetailsAndCheckCountInDatabase(operationType);
    }

    @Test(dataProvider = "addANewComputerToComputerDatabase", dataProviderClass = YantranetDataForOperation.class, groups = {"regression"})
    public void addANewComputerAndCancel(OperationType operationType) throws ParseException {
        createAComputerDetailsAndClickOnCancel(operationType);
    }
}
