package tests;

import pages.AddAComputerPage;
import pages.HomePage;
import utils.OperationType;

public class ValidationTests extends Yantranet{

    void chooseOperations(OperationType operationType){
        HomePage homePage = pageStore.get(HomePage.class);
        switch (operationType){
            case CREATE:
                rLogger.info("choose to create a computer in database");
                homePage.chooseToCreate();
                break;
            case UPDATE:
                rLogger.info("choose to edit a computer in database");
                homePage.chooseToUpdate();
                break;
        }
    }

    protected void validateCreatePageWithEmptyFieldsOrFields(String computerName){
        pageStore.get(HomePage.class).chooseToCreate();
        AddAComputerPage add = pageStore.get(AddAComputerPage.class);
        add.addAComputerName(computerName);
        add.assertionForEmptyFields();
    }
}
