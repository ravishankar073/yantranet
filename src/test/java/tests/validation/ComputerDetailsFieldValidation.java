package tests.validation;

import dataprovider.NegativeTestCases;
import org.testng.annotations.Test;
import tests.ValidationTests;
import tests.crud.handlers.Operations;

public class ComputerDetailsFieldValidation extends ValidationTests{

    @Test(dataProvider = "noData",dataProviderClass = NegativeTestCases.class)
    public void validateWithNoComputerDetails(String computerName){
        validateCreatePageWithEmptyFieldsOrFields(computerName);
    }
}
