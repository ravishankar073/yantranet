package listeners;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;
import tests.Yantranet;
import utils.Screenshot;

import java.io.File;


public class FailureListener extends TestListenerAdapter{
    private WebDriver driver;
    private static final String ESCAPE_PROPERTY = "org.uncommons.reportng.escape-output";

    @Override
    public void onTestFailure(ITestResult testResult) {
        driver = ((Yantranet)testResult.getInstance()).getDriver();
        if(!testResult.isSuccess()) {
            System.setProperty(ESCAPE_PROPERTY, "false");
            File capturedScreenshot = captureScreenshot();
            Screenshot screenshot = new Screenshot(testResult.getName());
            screenshot.saveScreenshot(capturedScreenshot);
            String screenshotPath = screenshot.getScreenshotPath();
            Reporter.setCurrentTestResult(testResult);
            Reporter.log(String.format("<a href='file:///%s' target='_blank'>screenshot <img src='file:///%s' height='512' width='512'/></a>",screenshotPath,screenshotPath),true);
            Reporter.setCurrentTestResult(null);
        }
    }

    private File captureScreenshot() {
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    }

}
