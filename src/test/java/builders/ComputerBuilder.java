package builders;


import entities.Computer;

public class ComputerBuilder {

    private Computer computer;

    public ComputerBuilder(){
        computer = new Computer();
        computer.setName("Mac book pro");
        computer.setIntroducedDate("2017-11-06");
        computer.setDiscontinuedDate("2018-11-06");
        computer.setCompany("Apple");
        computer.setEditComputerLink("/computers/501");
    }

    public ComputerBuilder withName(String name){
        computer.setName(name);
        return this;
    }

    public ComputerBuilder withIntroducedDate(String introducedDate){
        computer.setIntroducedDate(introducedDate);
        return this;
    }

    public ComputerBuilder withDiscontinuedDate(String discontinuedDate){
        computer.setDiscontinuedDate(discontinuedDate);
        return this;
    }

    public ComputerBuilder withCompany(String company){
        computer.setCompany(company);
        return this;
    }

    public ComputerBuilder withEditLink(String editLink){
        computer.setEditComputerLink(editLink);
        return this;
    }

    public Computer build(){
        return computer;
    }
}
