package utils;


import org.apache.commons.lang3.StringUtils;
import org.fluttercode.datafactory.impl.DataFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class RandomDataGenerator {

    public String getRandomName(){
        DataFactory df = new DataFactory();
        String[] names = new String[100];
        int min = 0;
        int max = 99;
        for (int i = min; i < max; i++) {
            names[i] = df.getFirstName() + " " + df.getLastName();
        }
        Random randomName = new Random();
        int randomNum = randomName.nextInt((max - min) + 1) + min;
        return names[randomNum];
    }

    public String getRandomComputer(){
        String[] computers = {"Apple", "Asus", "Acer", "Lenevo", "Dell"};
        Random randomComputer = new Random();
        int min = 0;
        int max = 4;

        int randomNum = randomComputer.nextInt((max - min) + 1) + min;
        return computers[randomNum];
    }

    public Date getRandomDate(Date startDate){
       return new DataFactory().getDate(startDate, 1, 30);
    }

    public String formatDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(date);
    }

    public int getRandomNumber() {
        Random randomNumber = new Random();
        int min = 0;
        int max = 8;
        int randomNum = randomNumber.nextInt(max - min) + min;
        return randomNum;
    }
}
