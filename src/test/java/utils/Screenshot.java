package utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class Screenshot {

    private String testName;
    private final String screenshotLocation = System.getProperty("user.dir")+"/build/screenshots";
    private String screenshotPath;

    public Screenshot(String testName) {
        this.testName = testName;
    }

    public void saveScreenshot(File screenshot) {
        try {
            createScreenshotsDirectory();
            File screenshotFile = new File(screenshotLocation, getFileName());
            FileUtils.copyFile(screenshot, screenshotFile);
            screenshotPath = screenshotFile.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getScreenshotPath() {
        return screenshotPath;
    }

    private String getFileName() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
        return testName + "_" + formatter.format(calendar.getTime()) + ".png";
    }

    private void createScreenshotsDirectory() {
        File screenshotFolder = new File(screenshotLocation);
        if (!screenshotFolder.exists()) {
            screenshotFolder.mkdir();
        }
    }
}
