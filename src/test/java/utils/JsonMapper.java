package utils;


import com.google.gson.Gson;
import entities.operationsdata.OperationsData;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class JsonMapper {
    private static Gson gson;
    private static Reader reader;

    private JsonMapper(){
        gson = new Gson();
        InputStream dataStream = getClass().getClassLoader().getResourceAsStream("Operations.json");
        reader = new BufferedReader(new InputStreamReader(dataStream));
    }

    public static <T> T getDetails(){
        new JsonMapper();
        return gson.fromJson(reader, (Class<T>) OperationsData.class);
    }
}
