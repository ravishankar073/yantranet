package utils;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Properties;

public class PropertiesHelper {

    private Properties properties;

    public PropertiesHelper(){
        properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream("yantranet.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getUrl(){
        return properties.getProperty("url");
    }

    public String getBrowser(){
        return properties.getProperty("browser");
    }

    public String getMode(){
        String mode = System.getProperty("mode");
        boolean modeDefined = StringUtils.isNoneBlank(mode);
        if (modeDefined){
            return mode;
        }else {
            return properties.getProperty("mode");
        }
    }

    public String getComputerNameToSearch(){
        String computerName = System.getProperty("computerNameToSearch");
        boolean computerNameDefined = StringUtils.isNoneBlank(computerName);
        if (computerNameDefined){
            return computerName;
        }else {
            return properties.getProperty("computerNameToSearch");
        }
    }

    public String getComputerNameToDelete(){
        String computerName = System.getProperty("computerNameToDelete");
        boolean computerNameDefined = StringUtils.isNoneBlank(computerName);
        if (computerNameDefined){
            return computerName;
        }else {
            return properties.getProperty("computerNameToDelete");
        }
    }

    public int getTimeout(){
        return Integer.parseInt(properties.getProperty("timeout"));
    }


}
