package utils;


public class Properties {

    private static PropertiesHelper propertiesHelper = new PropertiesHelper();

    public static String url = propertiesHelper.getUrl();
    public static String browser = propertiesHelper.getBrowser();
    public static String mode = propertiesHelper.getMode();
    public static int timeout = propertiesHelper.getTimeout();
    public static String computerNameToSearch = propertiesHelper.getComputerNameToSearch();
    public static String computerNameToDelete = propertiesHelper.getComputerNameToDelete();
}
