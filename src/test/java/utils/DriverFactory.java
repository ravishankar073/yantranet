package utils;


import org.apache.commons.exec.OS;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

import static utils.Properties.browser;

public class DriverFactory {

    private WebDriver driver;
    private String browserName;
    private String mode;

    public DriverFactory(){
        defineBrowserName();
        defineMode();
        setProperties();
        init();
    }

    public WebDriver getDriver(){
        return driver;
    }

    private void init(){
        switch (browserName.toLowerCase()){
            case "chrome":
                driver = new ChromeDriver();
                driver.manage().window().maximize();
                break;
            case "firefox":
                driver = new FirefoxDriver();
                driver.manage().window().maximize();
                break;
            case "safari":
                driver = new SafariDriver();
                driver.manage().window().maximize();
                break;
            default:
                throw new RuntimeException("Unsupported driver -- "+browserName);
        }
    }

    private void setProperties(){
        String driversLocation = "src/test/resources/drivers/";
        System.setProperty("org.uncommons.reportng.escape-output","false");

        if (OS.isFamilyMac()){
            System.setProperty("webdriver.gecko.driver",driversLocation+"geckodriver");
            System.setProperty("webdriver.chrome.driver",driversLocation+"chromedriver");
            System.setProperty("phantomjs.binary.path",driversLocation+"phantomjs");
        }else if(OS.isFamilyWindows()) {
            System.setProperty("webdriver.gecko.driver",driversLocation+"geckodriver.exe");
            System.setProperty("webdriver.chrome.driver",driversLocation+"chromedriver.exe");
        } else if(OS.isFamilyUnix()) {
            System.setProperty("webdriver.gecko.driver",driversLocation+"geckodriver_linux32");
            System.setProperty("webdriver.chrome.driver",driversLocation+"chromedriver_linux");
        }
    }



    private void defineBrowserName(){
        String browserName = System.getProperty("browser");
        boolean browserDefined = StringUtils.isNoneBlank(browserName);
        if (browserDefined){
            this.browserName = browserName;
        }else {
            this.browserName = browser;
        }
    }

    private void defineMode(){
        String mode = System.getProperty("mode");
        boolean modeDefined = StringUtils.isNoneBlank(mode);
        if (modeDefined){
            this.mode = mode;
        }else {
            this.mode = Properties.mode;
        }
    }

}
