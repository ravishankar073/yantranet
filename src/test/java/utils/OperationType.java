package utils;

public enum OperationType {
    CREATE,
    READ,
    UPDATE,
    DELETE
}
