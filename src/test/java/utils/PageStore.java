package utils;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class PageStore {

    private WebDriver driver;
    private List<Object> pages = new ArrayList();

    public PageStore(WebDriver driver){
        this.driver = driver;
    }

    public <T> T get(Class<T> clazz){
        for (Object page : pages){
            if (page.getClass().getSimpleName().contains(clazz.getSimpleName())){
                return (T) page;
            }
        }
        T page = null;
        try {
            page = PageFactory.initElements(driver, clazz);
            pages.add(page);
            return page;
        }catch (Exception e){
            return page;
        }
    }

    public WebDriver getDriver(){
        return driver;
    }
}
