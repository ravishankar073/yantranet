package utils;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import pages.YantranetPage;

import java.util.Arrays;

public class Assertions extends YantranetPage {

    @FindBy(css = ".fill a")
    private WebElement titleOfHomePage;



    private RLogger rLogger;

    public Assertions(WebDriver driver){
        super(driver);
        this.driver = driver;
        rLogger = new RLogger(this);
    }

    public void verifyHomePage(){
        waitForElementToBeVisible(titleOfHomePage, this);
        rLogger.info("Verifying title of application");
        rLogger.info(titleOfHomePage.getText());
        String[] titleParts = { "Play sample application", "Computer database" };
        for ( String part : titleParts ) {
            Assert.assertTrue(titleOfHomePage.getText().contains(part));
        }
    }



}

