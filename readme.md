# yantranet - Functional tests
Functional tests provides coverage for
*  Desktop Browsers


##Desktop Browsers include
* Chrome
* Firefox
* Safari **


##Functional Coverage Modules
* Create
* Read
* Update       
* Delete



##Steps to execute
Switch to master(yantranet)

    git checkout master

###Run gradle tasks

* Create



* All Functional Modules

    ```gradle clean build yantranet -Dbrowser=chrome/firefox```
    
###Results

View results in following path

    /build/reports/tests/FunctionalTestResults/html/index.html
    

